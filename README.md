# TestAnaliticaStrada

Analisis de sentimiento y reconocimiento de topicos para un conjunto de tweets

# Ambiente de desarrollo
  + Python 3.7.2

# Pasos a seguir

  + Installar librerias requeridas
    -   $ pip install -r requirements.tx
  + Ejecución del script
    -   $ python sentiments.py
    
# Datos resultantes

  Genera un csv llamado *analisis.csv*