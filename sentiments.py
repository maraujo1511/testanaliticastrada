# -*- coding: utf-8 -*-
import csv
import json
import pandas as pd
import re
import string
import collections as coll
import sys
import nltk


from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import *
# Instalamos los recursos de nltk
nltk.download('stopwords')
nltk.download('punkt')

# Se leen lor archivos que contienen las palabras poditivas y negativas
with open('positive-words.txt', 'r', encoding='utf8') as f:
    positivo = f.readlines()

for x in range(0,len(positivo)):
    positivo[x] = positivo[x].replace("\n", "").replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u").replace("ü", "u").replace("ñ", "n")

with open('negative-words.txt', 'r', encoding='utf8') as f:
    negativo = f.readlines()

for x in range(0,len(negativo)):
    negativo[x] = negativo[x].replace("\n", "").replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u").replace("ü", "u").replace("ñ", "n")

class topico():
    valor = 0
    topic = ""


def orden(x, y):
    if x.valor < y.valor:
        rst = -1
    elif x.valor > y.valor:
        rst = 1
    else:
        rst = 0
    return rst


def remover_puntuacion(text):
    url_re = re.compile('https?://(www.)?\w+\.\w+(/\w+)*/?')
    sig_pun = re.compile("[^0-9a-zA-Z]")
    #sin_pun_mas = re.compile(r'[¡¬ł€¶ŧ←↓→øþøæßðđŋħłµ¢»«¨]')
    punc_re = re.compile('[%s]' % re.escape(string.punctuation))
    
    
    num_re = re.compile('(\\d+)')
    mension_re = re.compile('@(\w+)')
    alpha_num_re = re.compile("^[a-z0-9_.]+$")
    # convert to lowercase
    #text = text.lower()
    # remover hiperlincks
    
    text = url_re.sub(' ', str(text))
    # remover @mension
    
    text = mension_re.sub(' ', str(text))
    # remover remover_puntuacion
    
    #text = sig_pun.sub(' ', str(text))
    # remover mas puntuacion
    
    # remover otra puntuacion
    
    text = punc_re.sub(' ', str(text))
    text = text.replace("\n", "").replace(u"\n", "").replace(r"\n", "").replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u").replace("ü", "u").replace("ñ", "n")
    
    # remover numerico
    #text = num_re.sub(' ', str(text))

    text = text.lower()
    list_pos = 0
    cleaned_str = ''
    for word in text.split():
        if (list_pos == 0):
            if alpha_num_re.match(word) and len(word) > 2:
                cleaned_str = word
            else:
                cleaned_str = ' '
        else:
            if alpha_num_re.match(word) and len(word) > 2:
                cleaned_str = cleaned_str + ' ' + word
            else:
                cleaned_str += ' '
        list_pos += 1
    return cleaned_str


def remove_stops(text):
    stops = set(stopwords.words("spanish"))
    list_pos = 0
    cleaned_str = ''
    spli = text.split()
    for word in spli:
        if word not in stops:
            if list_pos == 0:
                cleaned_str = word
            else:
                cleaned_str = cleaned_str + ' ' + word
            list_pos += 1
    return cleaned_str


def polaridad(text):
    
    pol = ""
    tokens = word_tokenize(text)    
    pos = map(lambda x: x in positivo, tokens)
    neg = map(lambda x: x in negativo, tokens)
    count_pos = coll.Counter(pos)[True]
    count_neg = coll.Counter(neg)[True]
    cien = count_pos + count_neg
    if cien > 0:
        if count_pos == count_neg:
            pol="NU"  # Neutral            
        else:
            if count_pos > count_neg:
                pol="P" #Positivo                
            else:
                pol="N" #NEgativo                
    else:
        pol="NU"
        
    return pol



def generar_topico(text):
    
    lista = []
    stemmer =  SnowballStemmer("spanish")
    ## Arreglos de polarides, segun criterios propios
    entre = ['entret', 'arte', 'cine', 'television', 'celebracion', 'teatr', 'espectacul', 'music', 'film', 'video',
             'novel', 'festival', 'carnaval', 'artista', 'cantante', 'cancion', 'obra', 'mod', 'pelicul', 'trail']  # 20
    religion = ['religion', 'dios', 'sant', 'islam', 'sacerdot', 'pecado', 'isis', 'templ', 'igles', 'musulman',
                'bibli', 'vatican', 'obizp', 'virg', 'feligres', 'pontific', 'santer', 'santeri', 'angel', 'catolic']  # 20
    deporte = ['deport', 'gan', 'equip', 'jug', 'jueg', 'part', 'tennis', 'fichaj', 'anot', 'anotacion', 'puntuacion',
               'puntaj', 'competent', 'futbol', 'beisbol', 'natacion', 'softbol', 'gol', 'carrer', 'campeonat', 'olimpi']  # 20
    educa = ['educ', 'educacion', 'aprend', 'activ', 'recurso', 'estudi', 'aula', 'peda', 'conoc',
             'ensen', 'univers', 'profesor', 'colegi', 'bec', 'lice', 'maestr', 'asignatur', 'graduacion']  # 20
    tecno = ['tecnolog', 'tecnologi', 'cienci', 'cientif', 'investig', 'investigacion', 'avanc', 'innov', 'innovacion', 'telefon',
             'internet', 'inform', 'informat', 'informatico', 'robot', 'comun', 'comput', 'celul', 'virtual', 'electron']  # 20
    econom = ['econom', 'economi', 'negoci', 'negociacion', 'financ', 'fianci', 'financiacion', 'empres', 'empresari',
              'inflacion', 'inversion', 'dolar', 'balanc', 'banc', 'merc', 'salari', 'monetari', 'moned', 'inflacion', 'prestam']  # 20
    salud = ['salud', 'enferm', 'enfermedad', 'enfermeri', 'virus', 'medicin', 'medic', 'medicament', 'inmun', 'bact',
             'bacteri', 'aliment', 'alimentacion', 'contag', 'contagi', 'infect', 'infeccion', 'paramed', 'cardi', 'hospital']  # 20
    politica = ['polit', 'dictadur', 'gobiern', 'gobern', 'alcald', 'alcaldes', 'ejercit', 'milit',
                'milician', 'dictador', 'eleccion', 'dictadur', 'gnb', 'democraci', 'cne', 'vot', 'anarqui']  # 20
    social = ['social', 'socied', 'civil', 'habit', 'asociacion', 'dialog', 'pais', 'mision', 'pobrez', 'derecho',
              'deb', 'onu', 'comuna', 'manifestacion', 'manifest', 'huelga', 'protest', 'disturbi', 'transit', 'march']
    tokens = word_tokenize(text)
    
    # realizo el map para las cocidencias de las palabras canonicas
    map_entre = map(lambda x: x in entre, [stemmer.stem(word) for word in tokens])
    map_religion = map(lambda x: x in religion, [stemmer.stem(word) for word in tokens])
    map_deporte = map(lambda x: x in deporte, [stemmer.stem(word) for word in tokens])
    map_educa = map(lambda x: x in educa, [stemmer.stem(word) for word in tokens])
    map_tecno = map(lambda x: x in tecno, [stemmer.stem(word) for word in tokens])
    map_econom = map(lambda x: x in econom, [stemmer.stem(word) for word in tokens])
    map_salud = map(lambda x: x in salud, [stemmer.stem(word) for word in tokens])
    map_politica = map(lambda x: x in politica, [stemmer.stem(word) for word in tokens])
    map_social = map(lambda x: x in social, [stemmer.stem(word) for word in tokens])

    # cuento las cantidad de veces que aparece para un topico en especifico
    # crea un objeto llamado topico()
    obj_entre = topico()
    obj_entre.valor = coll.Counter(map_entre)[True]
    obj_entre.topic = "Entretenimiento"
    obj_religion = topico()
    obj_religion.valor = coll.Counter(map_religion)[True]
    obj_religion.topic = "Religion"
    obj_deporte = topico()
    obj_deporte.valor = coll.Counter(map_deporte)[True]
    obj_deporte.topic = "Deporte"
    obj_educa = topico()
    obj_educa.valor = coll.Counter(map_educa)[True]
    obj_educa.topic = "Educacion"
    obj_tecno = topico()
    obj_tecno.valor = coll.Counter(map_tecno)[True]
    obj_tecno.topic = "Tecnologia"
    obj_econom = topico()
    obj_econom.valor = coll.Counter(map_econom)[True]
    obj_econom.topic = "Economia"
    obj_salud = topico()
    obj_salud.valor = coll.Counter(map_salud)[True]
    obj_salud.topic = "Salud"
    obj_politica = topico()
    obj_politica.valor = coll.Counter(map_politica)[True]
    obj_politica.topic = "Politica"
    obj_social = topico()
    obj_social.topic = "Social"
    obj_social.valor = coll.Counter(map_social)[True]

    # lista de todos los objetos de los topicos
    lista = [obj_entre, obj_religion, obj_deporte, obj_educa, obj_tecno, obj_econom, obj_salud, obj_politica, obj_social]
    
    # ordeno lal ista de mayor a menor segun su valor
    
    lista.sort(key=lambda b: b.valor, reverse=True)
    if (lista[0].valor == 0):
        return "Diverso"
    if (lista[0].topic == lista[1].topic or lista[1].valor == 0):
        return lista[0].topic
    else:
        return lista[0].topic+' - '+lista[1].topic

## INICIO DEL SCRIPT
# Leo el archico con padas para asi crear el data frame
df = pd.read_csv('twitter.txt', sep='>', encoding = 'utf-8')

df.columns = ["Tweet","Tweet_ID","Time","Favorited","Retweeted","Is_Favourited","Is_Retweeted","Is_Retweet","Retweet_from","Latitude","Longitude","Country","User","User_Name","User_ID","User_Description","User_Creation_time","User_Language","User_Location","User_Time_Zone","User_Statuses","User_Followers","User_Friends","User_Favourites","User_URL"]
# Se remueve la puntuacion de Tweet
df['Tweet'] = df['Tweet'].str.strip()
df['User_Description'] = df['User_Description'].str.strip()
df  = df.replace(u'\n',' ', regex=True)
df  = df.replace(r'\n',' ', regex=True)
df  = df.replace('\n',' ', regex=True)
df  = df.replace(',',' ', regex=True)
df['Tweet'] = df['Tweet'].str.replace(u"\n", " ")
df['Tweet'] = df['Tweet'].str.replace(r"\n", " ")
df['Tweet'] = df['Tweet'].str.replace("\n", " ")
df['User_Description'] = df['User_Description'].str.replace(u"\n", " ")
df['User_Description'] = df['User_Description'].str.replace(r"\n", " ")
df['User_Description'] = df['User_Description'].str.replace("\n", " ")
df['clear_punt'] = df['Tweet'].apply(remover_puntuacion)
df['No_Stop_Text'] = df['clear_punt'].apply(remove_stops)
# Generar la polaridad del Tweet
df['Polaridad']= df['clear_punt'].apply(polaridad)
#Generar el topico del Tweet
df['Topico'] = df['No_Stop_Text'].apply(generar_topico)

df.to_csv('analisis.csv', sep=',', quotechar='"',index=False, columns=["Tweet","Time","Favorited","Retweeted","Is_Favourited","Is_Retweeted","Is_Retweet","Retweet_from","Latitude","Longitude","Country","User","User_Name","User_ID","User_Description","User_Creation_time","User_Language","User_Location","User_Time_Zone","User_Statuses","User_Followers","User_Friends","User_Favourites","User_URL","No_Stop_Text","Polaridad","Topico"], encoding="utf-8", quoting=csv.QUOTE_MINIMAL)

print("Analisis finalizado")